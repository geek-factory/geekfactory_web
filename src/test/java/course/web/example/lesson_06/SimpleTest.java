package course.web.example.lesson_06;

import org.junit.jupiter.api.Test;

public class SimpleTest {

    //Example JUnit5
    @Test
    void testMethodJUnit5() {
        System.out.println("Simple test [JUnit5]");
    }

    //Example JUnit4
    @org.junit.Test
    public void testMethodJUnit4() {
        System.out.println("Simple test [JUnit4]");
    }
}
