package course.web.example.lesson_06;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    private SimpleOperation simpleCalculator;

    @BeforeAll
    static void beforeAll() {
        System.out.println("Before all tests (methods) called");
    }

    @BeforeEach
    void beforeEach() {
        System.out.println("Before each test (method) called");
        simpleCalculator = new Calculator();
    }

    @Test
    @DisplayName("Addition operation testing")
    void additionTest() {
        assertTrue(simpleCalculator != null);
        assertEquals(3, simpleCalculator.addition(1, 2));
    }

    @Test
    @DisplayName("Subtraction operation testing")
    void subtractionTest() {
        assertNotNull(simpleCalculator);
        assertEquals(3, simpleCalculator.subtraction(5, 2));
    }

    @Test
    @DisplayName("Multiplication operation testing")
    void multiplicationTest() {
        assertNotNull(simpleCalculator);
        assertEquals(3, simpleCalculator.multiplication(3, 1));
    }

    @Test
    @DisplayName("Division operation testing")
    void divisionTest() {
        assertNotNull(simpleCalculator);
        assertEquals(3, simpleCalculator.division(9, 3));
    }

    @Test
    @DisplayName("Division operation exception testing")
    void divisionExceptionTest() {
        assertThrows(ArithmeticException.class, () -> simpleCalculator.division(10, 0));
    }
}
