package course.web.example.lesson_06;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CalculatorParameterizedTest {

    private SimpleOperation simpleCalculator;

    @BeforeEach
    void beforeEach() {
        System.out.println("Before each test (method) called");
        simpleCalculator = new Calculator();
    }


    @DisplayName("Addition operation testing")
    @ParameterizedTest
    @CsvSource({"1, 1, 2",
            "2, 3, 5",
            "5, 5, 10",
            "-2, 2, 0"})
    void additionTest(int param1, int param2, int expectedResult) {
        assertEquals(expectedResult, simpleCalculator.addition(param1, param2));
    }

    @DisplayName("Subtraction operation testing")
    @ParameterizedTest
    @CsvSource({"2, 1, 1",
            "1, 1, 0",
            "-2, -2, 0",
            "2, -5, 7"})
    void subtractionTest(int param1, int param2, int expectedResult) {
        assertEquals(expectedResult, simpleCalculator.subtraction(param1, param2));
    }

    @DisplayName("Multiplication operation testing")
    @ParameterizedTest
    @CsvSource({"1, 1, 1",
            "2, 0, 0",
            "0, 2, 0",
            "2, 5, 10",
            "-2, 2, -4"})
    void multiplicationTest(int param1, int param2, int expectedResult) {
        assertEquals(expectedResult, simpleCalculator.multiplication(param1, param2));
    }

    @DisplayName("Division operation testing")
    @ParameterizedTest
    @CsvSource({"1, 1, 1",
            "1, 5, 0",
            "4, 2, 2",
            "0, 2, 0"})
    void divisionTest(int param1, int param2, int expectedResult) {
        assertEquals(expectedResult, simpleCalculator.division(param1, param2));
    }
}
