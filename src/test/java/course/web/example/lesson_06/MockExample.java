package course.web.example.lesson_06;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class MockExample {

    @InjectMocks
    private ServiceClass serviceClass;

    @Mock
    private SimpleOperation simpleCalculatorMock;

    @Spy
    Calculator spy;

    @Test
    void testServiceClass(){
        when(simpleCalculatorMock.addition(anyInt(), anyInt())).thenReturn(4);

        assertNotNull(serviceClass);
        assertEquals(4, serviceClass.method(1, 5));
        assertNotEquals(10, serviceClass.method(5, 5));

        verify(simpleCalculatorMock, times(2)).addition(anyInt(), anyInt());
        verify(simpleCalculatorMock, never()).multiplication(anyInt(), anyInt());

        assertEquals(0, simpleCalculatorMock.multiplication(1, 2));
        assertEquals(0, simpleCalculatorMock.division(1, 2));
        assertEquals(0, simpleCalculatorMock.subtraction(1, 2));
    }

    @Test
    void calculatorTest() {
        when(simpleCalculatorMock.addition(anyInt(), anyInt())).thenReturn(4);

        assertNotNull(simpleCalculatorMock);
        assertEquals(4, simpleCalculatorMock.addition(1, 6));
        assertNotEquals(10, simpleCalculatorMock.addition(5, 5));

        verify(simpleCalculatorMock, times(2)).addition(anyInt(), anyInt());
    }

    @Test
    void spyObjectTest() {
//        SimpleOperation simpleCalculator = new Calculator();
//        SimpleOperation spy = spy(simpleCalculator);
        when(spy.addition(anyInt(), anyInt())).thenReturn(4);

        assertEquals(4, spy.addition(5, 2));
        assertEquals(2, spy.subtraction(3, 1));
        assertEquals(2, spy.multiplication(1, 2));
        assertEquals(2, spy.division(4, 2));
    }
}
