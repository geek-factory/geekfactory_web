package course.web.example.lesson_06;

public class Runner {
    public static void main(String[] args) {
        int param1 = 10;
        int param2 = 5;

        SimpleOperation calculator = new Calculator();

        System.out.println(String.format("Addition of numbers: %d + %d = %d", param1, param2, calculator.addition(param1, param2)));
        System.out.println(String.format("Subtraction of numbers: %d - %d = %d", param1, param2, calculator.subtraction(param1, param2)));
        System.out.println(String.format("Multiplication of numbers: %d * %d = %d", param1, param2, calculator.multiplication(param1, param2)));
        System.out.println(String.format("Division of numbers:  %d % %d = %d", param1, param2, calculator.division(param1, param2)));
    }
}
