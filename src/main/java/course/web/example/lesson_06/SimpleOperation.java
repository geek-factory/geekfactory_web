package course.web.example.lesson_06;

public interface SimpleOperation {
    int addition(int param1, int param2);

    int subtraction(int param1, int param2);

    int multiplication(int param1, int param2);

    int division(int param1, int param2);
}
