package course.web.example.lesson_06;

public class Calculator implements SimpleOperation {

    public int addition(int param1, int param2) {
        return param1 + param2;
    }

    public int subtraction(int param1, int param2) {
        return param1 - param2;
    }

    public int multiplication(int param1, int param2) {
        return param1 * param2;
    }

    public int division(int param1, int param2) {
        return param1 / param2;
    }
}
