package course.web.example.lesson_06;

public class ServiceClass {

    private SimpleOperation simpleCalculator;

    public ServiceClass() {
        simpleCalculator = new Calculator();
    }

    int method(int a, int b) {
        return simpleCalculator.addition(a, b);
    }
}
