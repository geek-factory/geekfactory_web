CREATE TABLE author_tbl (
  author_id INT AUTO_INCREMENT PRIMARY KEY,
  author_name VARCHAR(50)
);

INSERT INTO author_tbl (author_name) VALUES
  ('Harper Lee');

INSERT INTO author_tbl (author_name) VALUES
  ('Jane Austen');

INSERT INTO author_tbl (author_name) VALUES
  ('George Orwell');

CREATE TABLE book_tbl (
  book_id INT AUTO_INCREMENT PRIMARY KEY,
  book_name VARCHAR(50),
  author_id INT,

  CONSTRAINT AUTHOR_FK FOREIGN KEY (author_id) REFERENCES author_tbl (author_id)
);

INSERT INTO book_tbl (book_name, author_id) VALUES
  ('To Kill a Mockingbird', 1),
  ('Pride and Prejudice', 2),
  ('Animal Farm', 3);


CREATE TABLE customers (
  customer_id INT AUTO_INCREMENT PRIMARY KEY,
  customer_name VARCHAR(200)
);

CREATE TABLE orders (
  order_id INT AUTO_INCREMENT PRIMARY KEY,
  amount DOUBLE,
  customer_id INT,

  FOREIGN KEY (customer_id) REFERENCES customers(customer_id)
);

CREATE TABLE items (
  item_id INT AUTO_INCREMENT PRIMARY KEY,
  item_name VARCHAR(100)
);

CREATE TABLE items_orders (
  order_id INT,
  item_id INT,
  FOREIGN KEY (order_id) REFERENCES orders(order_id),
  FOREIGN KEY (item_id) REFERENCES items(item_id)
);

INSERT INTO customers (customer_id, customer_name) VALUES
  (1, 'Adam'),
  (2, 'Andy'),
  (3, 'Joe'),
  (4, 'Sandy');

INSERT INTO orders (order_id, customer_id, amount) VALUES
  (1, 1, 19.99),
  (2, 1, 35.15),
  (3, 3, 17.56),
  (4, 4, 12.34);

INSERT INTO items (item_id, item_name) VALUES
  (1, 'Playstation 4'),
  (2, 'Effective Java'),
  (3, 'PC'),
  (4, 'Toy');

INSERT INTO items_orders(item_id, order_id) VALUES
  (1, 1),
  (1, 2),
  (1, 3),
  (1, 4),
  (2, 2),
  (2, 4),
  (3, 3),
  (4, 1),
  (4, 4);

# INNER JOIN
SELECT customer_name, amount
FROM customers c INNER JOIN orders o ON c.customer_id = o.customer_id

# LEFT JOIN
SELECT customer_name, amount
FROM customers c LEFT JOIN orders o ON c.customer_id = o.customer_id

# RIGHT JOIN
SELECT customer_name, amount
FROM customers c RIGHT JOIN orders o ON c.customer_id = o.customer_id

# Join All Tables
SELECT DISTINCT customer_name, item_name, o.order_id
FROM
  customers c
  JOIN orders o ON c.customer_id = o.customer_id
  JOIN items_orders io ON o.order_id = io.order_id
  JOIN items i ON io.item_id = i.item_id