package course.web.example.lesson_07.sax;

import course.web.example.lesson_07.model.Employee;
import course.web.example.lesson_07.Parser;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.List;

public class SaxParser implements Parser<Employee> {
    @Override
    public List<Employee> parseEmployes(String xmlFile) throws IOException, SAXException, ParserConfigurationException {
        SAXParserFactory parserFactor = SAXParserFactory.newInstance();
        SAXParser parser = parserFactor.newSAXParser();
        SAXHandler handler = new SAXHandler();
        parser.parse(ClassLoader.getSystemResourceAsStream(xmlFile), handler);

        return handler.employeeList;
    }
}
