package course.web.example.lesson_07.sax;

import course.web.example.lesson_07.model.Employee;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class SAXHandler extends DefaultHandler {

    List<Employee> employeeList = new ArrayList<>();
    Employee employee = null;
    String content = null;

    @Override
    //Triggered when the start of tag is found.
    public void startElement(String uri, String localName, String qName, Attributes attributes)
            throws SAXException {

        switch(qName){
            //Create a new Employee object when the start tag is found
            case "employee":
                employee = new Employee();
                employee.setId(attributes.getValue("id"));
                break;
        }
    }

    @Override
    public void endElement(String uri, String localName,
                           String qName) throws SAXException {
        switch(qName){
            //Add the employee to list once end tag is found
            case "employee":
                employeeList.add(employee);
                break;
            //For all other end tags the employee has to be updated.
            case "firstName":
                employee.setFirstName(content);
                break;
            case "middleName":
                employee.setMiddleName(content);
                break;
            case "lastName":
                employee.setLastName(content);
                break;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length)
            throws SAXException {
        content = String.copyValueOf(ch, start, length).trim();
    }
}
