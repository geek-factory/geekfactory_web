package course.web.example.lesson_07.stax;

import course.web.example.lesson_07.model.Employee;
import course.web.example.lesson_07.Parser;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class StaxParser implements Parser<Employee> {

    @Override
    public List<Employee> parseEmployes(String xmlFile) throws IOException, SAXException, ParserConfigurationException, XMLStreamException {
        List<Employee> employeeList = null;
        Employee currEmp = null;
        String tagContent = null;
        XMLInputFactory factory = XMLInputFactory.newInstance();
        XMLStreamReader reader =
                factory.createXMLStreamReader(ClassLoader.getSystemResourceAsStream(xmlFile));

        while(reader.hasNext()){
            int event = reader.next();

            switch(event){
                case XMLStreamConstants.START_ELEMENT:
                    if ("employee".equals(reader.getLocalName())){
                        currEmp = new Employee();
                        currEmp.setId(reader.getAttributeValue(0));
                    }
                    if("employees".equals(reader.getLocalName())){
                        employeeList = new ArrayList<>();
                    }
                    break;

                case XMLStreamConstants.CHARACTERS:
                    tagContent = reader.getText().trim();
                    break;

                case XMLStreamConstants.END_ELEMENT:
                    switch(reader.getLocalName()){
                        case "employee":
                            employeeList.add(currEmp);
                            break;
                        case "firstName":
                            currEmp.setFirstName(tagContent);
                            break;
                        case "middleName":
                            currEmp.setMiddleName(tagContent);
                            break;
                        case "lastName":
                            currEmp.setLastName(tagContent);
                            break;
                    }
                    break;

                case XMLStreamConstants.START_DOCUMENT:
                    employeeList = new ArrayList<>();
                    break;
            }
        }

        return employeeList;
    }
}
