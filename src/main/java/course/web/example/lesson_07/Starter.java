package course.web.example.lesson_07;

import course.web.example.lesson_07.dom.DOMParser;
import course.web.example.lesson_07.model.Employee;
import course.web.example.lesson_07.sax.SaxParser;
import course.web.example.lesson_07.stax.StaxParser;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.util.List;

public class Starter {
    public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException, XMLStreamException {

        long start = System.currentTimeMillis();
        Parser<Employee> domParser = new DOMParser();
        List<Employee> employeeListDOM = domParser.parseEmployes("employee.xml");
        domParser.printEntryList(employeeListDOM);
        long end = System.currentTimeMillis() - start;
        System.out.println(end + " milliseconds");

        System.out.println("----------------------------------------------------------------------------------");

        start = System.currentTimeMillis();
        Parser<Employee> saxParser = new SaxParser();
        List<Employee> employeeListSAX = saxParser.parseEmployes("employee.xml");
        saxParser.printEntryList(employeeListSAX);
        end = System.currentTimeMillis() - start;
        System.out.println(end + " milliseconds");

        System.out.println("----------------------------------------------------------------------------------");

        start = System.currentTimeMillis();
        Parser<Employee> staxParser = new StaxParser();
        List<Employee> employeeListSTAX = staxParser.parseEmployes("employee.xml");
        staxParser.printEntryList(employeeListSTAX);
        end = System.currentTimeMillis() - start;
        System.out.println(end + " milliseconds");

    }
}
